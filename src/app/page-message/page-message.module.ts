import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageMessageRoutingModule } from './page-message-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PageMessageRoutingModule
  ]
})
export class PageMessageModule { }
