import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageLoaderRoutingModule } from './page-loader-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PageLoaderRoutingModule
  ]
})
export class PageLoaderModule { }
